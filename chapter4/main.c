#include <stdio.h>

int main() {
    FILE *file;
    int table[9][9];
    int i, j, k;
    int n;

    file = fopen("sudoku.txt", "r");
    for (i = 0; i < 9; i++) {
        for (j = 0; j < 9; j++) {
                fscanf(file, "%d", &table[i][j]);
        }
    }
    fclose(file);

    for (i = 0; i < 9; i++) {
        // Print horizontal division between block
        if (i % 3 == 0) {
            // Print '+---+---+---+'
            for (k = 0; k < 9; k++) {
                if (k % 3 == 0) {
                    printf("+");
                }
                printf("-");
            }
            printf("+\n");
        }

        for (j = 0; j < 9; j++) {
            // Print vertical division between block
            if (j % 3 == 0) {
                    printf("|");
            }

            if (table[i][j] == 0) {
                printf("#");
            }
            else {
                printf("%d", table[i][j]);
            }
        }
        printf("|\n");
    }

    // Print '+---+---+---+'
    for (k = 0; k < 9; k++) {
        if (k % 3 == 0) {
                printf("+");
        }
        printf("-");
    }
    printf("+\n");
}
