#include <stdio.h>

int main() {
    int i, j, k;
    int n;

    for (i = 0; i < 9; i++) {
        // Print horizontal division between block
        if (i % 3 == 0) {
            // Print '+---+---+---+'
            for (k = 0; k < 9; k++) {
                if (k % 3 == 0) {
                    printf("+");
                }
                printf("-");
            }
            printf("+\n");
        }

        for (j = 0; j < 9; j++) {
            // Print vertical division between block
            if (j % 3 == 0) {
                    printf("|");
            }

            printf("#");
        }
        printf("|\n");
    }

    // Print '+---+---+---+'
    for (k = 0; k < 9; k++) {
        if (k % 3 == 0) {
                printf("+");
        }
        printf("-");
    }
    printf("+\n");
}
