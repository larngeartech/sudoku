#include <stdio.h>

#define SIZE  9
#define BLOCK 3

typedef struct t_position {
  int row;
  int col;
} position;

typedef enum { false, true } bool;

// An array represents sudoku table, number 0 represents blank position.
int table[SIZE][SIZE];

// An array to keep track of usage of number 1-9 in table
// Thus, the 0th index is not used
bool used_row[SIZE][SIZE+1];
bool used_col[SIZE][SIZE+1];
bool used_block[BLOCK][BLOCK][SIZE+1];

// Print '+---+---+---+'
void print_horizontal_border() {
  int i;
  for (i = 0; i < SIZE; i++) {
    if (i % BLOCK == 0) {
      printf("+");
    }
    printf("-");
  }
  printf("+\n");
}

// Print sudoku table
void print_table(int table[SIZE][SIZE]) {
  int i, j;
  for (i = 0; i < SIZE; i++) {
    // Print horizontal division between block
    if ((i % BLOCK) == 0) {
      print_horizontal_border();
    }

    for (j = 0; j < SIZE; j++) {
      // Print vertical division between block
      if ((j % BLOCK) == 0) {
        printf("|");
      }

      // Print number or # for blank
      if (table[i][j] == 0) {
        printf("#");
      } else {
        printf("%d", table[i][j]);
      }
    }
    printf("|\n");
  }
  print_horizontal_border();
}

// Read sudoku table from a file specified by input_file name
// and print input table to console
void read_file(char* filename) {
  int i, j;
  // Read input
  //<< move declaration here
  FILE* file = fopen(filename, "r");
  for (i = 0; i < SIZE; i++) {
    for (j = 0; j < SIZE; j++) {
      fscanf(file, "%d", &table[i][j]);
    }
  }
  fclose(file);
}

void init_used() {
  int i, j;
  // Init state table to represent no number has been used before
  for (i = 0; i < SIZE; i++) {
    for (j = 0; j < SIZE; j++) {
      int n = table[i][j];
      bool b = n != 0;
      used_row[i][n] = b;
      used_col[j][n] = b;
      used_block[i/BLOCK][j/BLOCK][n] = b;
    }
  }
}

// Check whether number specified by candidate can be used
// to fill table[row][col] position according to sudoku rules.
bool can_fill(int candidate, position pos) {
  return !used_row[pos.row][candidate] &&
         !used_col[pos.col][candidate] &&
         !used_block[pos.row/BLOCK][pos.col/BLOCK][candidate];
}

// Return next blank position start searching from table[row][col]
position find_blank_pos(int row, int col) {
  int i, j;
  for (i = row; i < SIZE; i++) {
    for (j = col; j < SIZE; j++) {
      if (table[i][j] == 0) {
        return (position) {i, j};
      }
    }
    col = 0;
  }
  return (position) {-1, -1};
}


void set_used(int candidate, position pos, bool b) {
  used_row[pos.row][candidate] = b;
  used_col[pos.col][candidate] = b;
  used_block[pos.row/BLOCK][pos.col/BLOCK][candidate] = b;
}

void init(char* filename) {
  read_file(filename);
  init_used();

  printf("%s::\n", filename);
  print_table(table);
}

// Recursively fill table starting from next blank position after (row,col) position.
void fill_table(int row, int col) {
  position blank_pos = find_blank_pos(row, col);
  // Terminate if no blank left
  if (blank_pos.row == -1) {
    printf("solution::\n");
    print_table(table);
  } else {
    // Try filling the blank position with number 1-9
    int candidate;
    for (candidate = 1; candidate <= SIZE; candidate++) {
      if (can_fill(candidate, blank_pos)) {
        // Fill the blank position with candidate number
        table[blank_pos.row][blank_pos.col] = candidate;
        set_used(candidate, blank_pos, true);
        // Recursive
        fill_table(blank_pos.row, blank_pos.col);
        // Backtracking
        table[blank_pos.row][blank_pos.col] = 0;
        set_used(candidate, blank_pos, false);
      }
    }
  }
}

//<< change declaration
int main(int argc, char* argv[]) {
  init(argv[1]);
  // Fill table starting from position table[0][0]
  fill_table(0, 0);

  return 0;
}
